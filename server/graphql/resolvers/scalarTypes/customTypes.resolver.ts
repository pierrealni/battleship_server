import { GraphQLScalarType } from 'graphql';
import { Kind } from 'graphql/language';
import * as GraphQLJSON from 'graphql-type-json';

const DateType = new GraphQLScalarType({
  name: 'Date',
  description: 'Date custom scalar type',
  // value from the client
  parseValue(value) {
    return new Date(value);
  },
  // value sent to the client
  serialize(value) {
    return value;
  },
  // ast value is always in string format
  parseLiteral(ast) {
    if (ast.kind === Kind.INT) {
      return new Date(ast.value);
    }

    return null;
  }
});

const ObjectIdType = new GraphQLScalarType({
  name: 'ObjectId',
  description: 'ObjectId custom scalar type',
  // value sent to the client
  serialize(value) {
    return value.toString();
  }
});

export const customTypesResolver = {
  Date: DateType,
  JSON: GraphQLJSON,
  ObjectID: ObjectIdType
};
