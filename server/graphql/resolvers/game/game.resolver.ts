import { IResolvers } from 'graphql-tools';
import { PubSub, withFilter } from 'graphql-subscriptions';
import * as moment from 'moment';
import { turnOptions, getNewStatusDesc, isGameFinished } from '../../../helpers/game/util';
import { cellStatus } from '../../../constants';
import { putShipsOnCells } from '../../../helpers/game/ships';
import { dbQueries, dbWrites, dbUpdates } from '../../../dbOperations';

const pubsub = new PubSub();

export const gameResolver: IResolvers = {
  Query: {
    fetchTest: async () => {
      return await dbQueries.fetchActiveGames();
    },
    fetchGames: async () => {
      return await dbQueries.fetchActiveGames();
    },
    fetchMyFinishedGames: async (root, { id }) => {
      const gamesArray = await dbQueries.fetchGames();

      if (gamesArray) {
        return gamesArray.filter((game: any) => game.status !== 'active' && (game.player1.id === id || game.player2.id === id));
      }
      return null;
    },
    fetchGame: async (root, { id }) => {
      return await dbQueries.fetchGame(id);
    },
    fetchPlayers: async () => {
      return await dbQueries.fetchPlayers();
    },
    fetchPlayerLogin: async (root, { name, password }) => {
      const player = await dbQueries.fetchPlayerByName(name) as any;

      if (player && player.password === password) {
        return player;
      }

      return null;
    },
    fetchPlayer: async (root, { id }) => {
      return await dbQueries.fetchPlayer(id);
    },
  },
  Mutation: {
    startGame: async (root, { game }) => {
      const { id, playerId } = game;
      const player1 = await dbQueries.fetchPlayer(playerId) as any;
      let newMatrix;
      let newGame;

      if (!player1)
        throw new Error('Player does not exist');

      newMatrix = await putShipsOnCells();
      newGame = {
        id,
        settings: { matrix1: newMatrix },
        player1: { id: player1.id, name: player1.name },
        player2: turnOptions.noOpponent,
        currentTurn: turnOptions.noOpponent,
        turnsPlayed: 0,
        status: 'active',
        createdAt: moment().toDate().toString()
      };
      // Let's put a new entry to the db
      await dbWrites.createGame(newGame);
      // Let's publish the new game
      pubsub.publish('gameAdded', { gameAdded: newGame });

      return { response: 'Create Game in server success', matrix: newMatrix };
    },
    joinGame: async (root, { game }) => {
      const { id, playerId } = game;
      const { key, _game } = await dbQueries.fetchGameWithKey(id);
      let player2;
      let newMatrix;
      let actionBase;
      let _gameUpdates;
      let action_to_p1;
      let action_to_p2;

      if (!_game)
        throw new Error('Game does not exist');
      player2 = await dbQueries.fetchPlayer(playerId);
      if (!player2)
        throw new Error('Player does not exist');

      _gameUpdates = {};
      newMatrix = await putShipsOnCells();
      _gameUpdates.player2 = { id: player2.id, name: player2.name };
      _gameUpdates.settings = { matrix1: _game.settings.matrix1, matrix2: newMatrix };
      _gameUpdates.currentTurn = _gameUpdates.player2;
      actionBase = { actionDesc: `${player2.name} joined the game`, key: Date.now(), actorId: playerId };
      //Let's update the db
      await dbUpdates.updateGame(key, _gameUpdates);
      // Let's publish the new action. This can be listened only by player 1
      action_to_p1 = { ...actionBase, nextTurnId: player2.id, opponentName: _gameUpdates.player2.name };
      pubsub.publish('actionAdded', { actionAdded: action_to_p1, gameId: id });
      // This is returned to player 2
      action_to_p2 = { ...actionBase, opponentName: _game.player1.name };

      return { response: 'Join game in server success', actionAdded: action_to_p2, matrix: newMatrix };
    },
    surrenderGame: async (root, { surrender }) => {
      const { gameId, playerId } = surrender;
      const { key, _game }: any = await dbQueries.fetchGameWithKey(gameId);
      let player;
      let _gameUpdates;
      let actionDesc;
      let actionAdded;

      if (!_game)
        throw new Error('Game does not exist');
      player = await dbQueries.fetchPlayer(playerId);
      if (!player)
        throw new Error('Player does not exist');

      _gameUpdates = {};
      _gameUpdates.status = `${player.name} surrendered`;
      if (_game.player2.id === '-1') {
        _gameUpdates.player2 = turnOptions.surrendered;
      }
      actionDesc = `${player.name} surrendered the game`;
      // Let's update the db
      await dbUpdates.updateGame(key, _gameUpdates);
      // Let's publish the new action
      actionAdded = { actionDesc, key: Date.now(), gameIsSurrendered: true, actorId: playerId };
      pubsub.publish('actionAdded', { actionAdded, gameId });

      return 'Surrender in server success';
    },
    makeMove: async (root, { move }) => {
      const { gameId, x, y, newStatus, playerIndex } = move;
      const { key, _game }: any = await dbQueries.fetchGameWithKey(gameId);
      let auxMatrix = null;
      let actionDesc = '';
      let _gameUpdates: any = {};
      let actor = { id: '', name: '' };
      let _newStatus;
      let actionAdded;

      if (!_game)
        throw new Error('Game does not exist');

      switch (true) {
        case playerIndex === 1:
          auxMatrix = _game.settings.matrix1;
          await dbUpdates.updateCell(key, 1, x, y, newStatus);
          actionDesc = `${_game.player1.name} clicked x: ${x}, y: ${y} `;
          actor = _game.player1;
          _gameUpdates.currentTurn = { id: _game.player2.id, name: _game.player2.name };
          break;
        case playerIndex === 2:
          auxMatrix = _game.settings.matrix2;
          await dbUpdates.updateCell(key, 2, x, y, newStatus);
          actionDesc = `${_game.player2.name} clicked x: ${x}, y: ${y} `;
          actor = _game.player2;
          _gameUpdates.currentTurn = { id: _game.player1.id, name: _game.player1.name };
          break;
        default:
          throw new Error('Player index is incorrect');
      }
      actionDesc += getNewStatusDesc(newStatus);
      _gameUpdates.turnsPlayed = _game.turnsPlayed + 1;
      // If the player wins the game
      if (_gameUpdates.turnsPlayed >= 39 && newStatus === cellStatus.HITTED) {
        auxMatrix[x][y] = newStatus;
        if (isGameFinished(auxMatrix)) {
          _newStatus = `${actor.name} wins`;
          _gameUpdates.status = _newStatus;
          actionDesc = _newStatus;
          // Let's update the db
          await dbUpdates.updateGame(key, _gameUpdates);
          //Let's publish the new action
          actionAdded = { actionDesc, key: Date.now(), gameHasWinner: true, actorId: actor.id };
          pubsub.publish('actionAdded', { actionAdded, gameId });

          return `${actionDesc} in server: success`;
        }
      }
      // If game continues, Let's update the db
      await dbUpdates.updateGame(key, _gameUpdates);
      // If game continues, Let's publish a regular new action
      actionAdded = { nextTurnId: _gameUpdates.currentTurn.id, actionDesc, key: Date.now(), actorId: actor.id };
      pubsub.publish('actionAdded', { actionAdded, gameId });

      return `${actionDesc} in server: success`;
    },
    createPlayer: async (root, { player }) => {
      const { name, password } = player;
      const existingPlayer = await dbQueries.fetchPlayerByName(name);
      let newPlayer;

      if (existingPlayer) {
        return { id: '-1', name: '' };
      }
      newPlayer = { id: String(Date.now()), name, password };
      // Let's put a new entry to the db
      await dbWrites.createPlayer(newPlayer);

      return newPlayer;
    }
  },
  Subscription: {
    gameAdded: { subscribe: () => pubsub.asyncIterator('gameAdded') },
    actionAdded: {
      subscribe: withFilter(() => pubsub.asyncIterator('actionAdded'), (payload, variables) => {
        return payload.gameId === variables.gameId;
      }),
    }
  },
};
