import * as express from 'express';
import { ApolloServer } from 'apollo-server-express';
import { schema } from './graphql';
import * as http from 'http';
import { PORT } from './settings';

export const startServer = async () => {
  const app = express();

  const server = new ApolloServer({
    schema: schema
  });

  server.applyMiddleware({
    app,
    cors: true
  });

  const httpServer = http.createServer(app);
  server.installSubscriptionHandlers(httpServer);

  httpServer.listen({port: PORT}, () => {
    console.log(`🚀 Server ready at http://localhost:3030${server.graphqlPath}`);
    console.log(`🚀 Subscriptions ready at http://localhost:3030${server.subscriptionsPath}`);
  });
};

startServer();