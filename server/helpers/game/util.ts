import { cellStatus } from "../../constants";

export const turnOptions = {
  noOpponent: { id: '-1', name: 'Pending, game without opponent' },
  surrendered: { id: '-2', name: 'No player' }
};

export const isGameFinished = (matrix) => {
  let isFinished = true;

  for(let i = 0; i <10; i++){
    for(let j = 0; j <10; j++){
      if(matrix[i][j] > 0 ){
        isFinished = false;

        return isFinished;
      }
    }
  }

  return isFinished;
};

export const getNewStatusDesc = (newStatus) => {
  let actionDesc: String = '';
  switch(newStatus){
    case (cellStatus.HITTED):
      actionDesc += ' and hit a ship';
      break;
    case (cellStatus.MISS):
      actionDesc += ' and missed';
      break;
    default:
      throw new Error('New status is incorrect');
  }

  return actionDesc;
};
