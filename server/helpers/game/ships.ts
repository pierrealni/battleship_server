import { cellStatus, maxPositions, defaultGameMatrix, ships } from '../../constants';

export const putShipsOnCells = () => (
  new Promise((resolve) => {
    const maxX = maxPositions.X;
    const maxY = maxPositions.Y;
    const newMatrix = defaultGameMatrix.map(row => row.slice());
    let position;
    let initialX;
    let initialY;
    let overlap;
    let i;

    ships.forEach((ship) => {
      position = Math.floor(Math.random() * 2); // 0 -> horizontal. 1 -> vertical.
      initialX = 0;
      initialY = 0;
      overlap = false;

      do {
        overlap = false;
        initialX = Math.floor(Math.random() * maxX);
        initialY = Math.floor(Math.random() * maxY);

        if (position) { // vertical check.
          for (i = 0; i < ship.size; i += 1) {
            if ((initialY + i > maxY) || (newMatrix[initialX][initialY + i] > cellStatus.WATER)) {
              overlap = true;
            }
          }
        } else { // horizontal check.
          for (i = 0; i < ship.size; i += 1) {
            if ((initialX + i > maxX) || (newMatrix[initialX + i][initialY] > cellStatus.WATER)) {
              overlap = true;
            }
          }
        }
      }
      while (overlap);

      if (position) { // vertical place.
        for (i = 0; i < ship.size; i += 1) {
          newMatrix[initialX][initialY + i] = ship.id;
        }
      } else { // horizontal place.
        for (i = 0; i < ship.size; i += 1) {
          newMatrix[initialX + i][initialY] = ship.id;
        }
      }
    });

    resolve(newMatrix);
  })
);
