import * as firebase from 'firebase';
import { FIREBASECONF } from "./settings";

(firebase as any).initializeApp(FIREBASECONF);
const db = (firebase as any).database();

export const dbQueries = {
  fetchPlayers: async () => {
    const players = await db.ref('/players').once('value');

    return players.val() ? Object.values(players.val()) : null;
  },
  fetchPlayer: async (id) => {
    const player = await db.ref('/players').orderByChild('id').equalTo(id).once('value');

    return player.val() ? Object.values(player.val())[0] : null;
  },
  fetchPlayerByName: async (name) => {
    const player = await db.ref('/players').orderByChild('name').equalTo(name).once('value');
    //return player.val()[0];
    return player.val() ? Object.values(player.val())[0] : null;
  },
  fetchActiveGames: async () => {
    const games = await db.ref('/games').orderByChild('status').equalTo('active').once('value');

    return games.val() ? Object.values(games.val()) : null;
  },
  fetchGames: async () => {
    const games = await db.ref('/games').once('value');

    return games.val() ? Object.values(games.val()) : null;
  },
  fetchGame: async (id) => {
    const game = await db.ref('/games').orderByChild('id').equalTo(id).once('value');

    return game.val() ? Object.values(game.val())[0] : null;
  },
  fetchGameWithKey: async (id) => {
    const game = await db.ref('/games').orderByChild('id').equalTo(id).once('value');
    const gameJson: any = game.toJSON();
    let key;

    if (gameJson) {
      key = Object.keys(gameJson)[0];
      return { key: key, _game: gameJson[key] };
    }

    return null;
  },
};

export const dbWrites = {
  createGame: async (newGame) => {
    return await db.ref('/games').push(newGame);
  },
  createPlayer: async (newPlayer) => {
    return await db.ref('/players').push(newPlayer);
  }
};

export const dbUpdates = {
  updateGame: async (key, updateObj) => {
    return await db.ref(`/games/${key}`).update(updateObj);
  },
  updateCell: async (gameKey, matrixIndex, x, y, newStatus) => {
    return await db.ref(`/games/${gameKey}/settings/matrix${matrixIndex}/${x}/${y}`).set(newStatus);
  },
};
