This is the server side for the real-time version of the Battleship Game. Details Below.

---

# Get Started


**1. Clone the project**


    git clone https://pierrealni@bitbucket.org/pierrealni/battleship_server.git


**2. Install dependencies**


    npm install


**3. Run the  app**


    npm start

    This will run the automated build process and start up the server in the port 3030


---

# GraphQL interface

![Alt text](https://i.imgur.com/EkP7H8l.png "Server Preview 1")

---

# Features

* **Queries**
    * fetchGame(id: ID!): Game
    * fetchGames: [Game]
    * fetchMyFinishedGames(id: ID!): [Game]
    * fetchPlayers: [Player]
    * fetchPlayerLogin(name: String!, password: String!): Player
    * fetchPlayer(id: ID!): Player

* **Mutations**
    * startGame(game: NewGameInput!): NewGameOutput
    * surrenderGame(surrender: SurrenderInput!): String
    * joinGame(game: NewGameInput!): NewGameOutput
    * makeMove(move: MoveInput!): String
    * createPlayer(player: PlayerInput!): Player

* **Subscriptions**
    * gameAdded: Game
    * actionAdded(gameId: ID!): JSON

---

# Technologies


This part of the Battleship App is the result of the sum of the following technologies:


| **Name**  | **Description**|
|---------- |----------------|
| Express | Serves development and production builds |
| Apollo Server | Library meant to be the best way to quickly build a production-ready, self-documenting API for GraphQL clients |
| Firebase Database| Google product used here with the purpose of static data |
| TypeScript | Strict syntactical superset of JavaScript |
